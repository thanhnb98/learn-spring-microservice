package com.thanhnb.currentcyexchangeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrentcyExchangeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrentcyExchangeServiceApplication.class, args);
	}

}
