package com.thanhnb.organizationservice.controller;

import com.thanhnb.organizationservice.events.models.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.couchbase.CouchbaseProperties;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/organizations")
public class DemoDiscoveryClient {

    @Value("${server.port}")
    private String port;

    @RequestMapping(value = "/{organizationID}")
    public String getOrganInfo(@PathVariable("organizationID") String val) {
        return "getOrganInfo() " + val + "=> " + port;
    }


    @RequestMapping(value = "/getOrganInfo2/{organizationID}")
    public Organization getOrganInfo2(@PathVariable("organizationID") String val) {
        Organization o = new Organization();
        o.setField1("Field1");
        o.setField2("Field2");
        o.setField3("Field3");
        o.setField4("Field4");
        o.setField5("Field5");
        return o;
    }
}
