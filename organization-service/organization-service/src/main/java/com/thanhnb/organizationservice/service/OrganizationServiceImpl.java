package com.thanhnb.organizationservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thanhnb.organizationservice.events.source.SourceBean;
import com.thanhnb.organizationservice.inputs.OrganInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationServiceImpl implements IOrganizationService {
    private static final String COMMENT = "################################";
    private static final Logger log = LoggerFactory.getLogger(OrganizationServiceImpl.class);

    @Autowired
    private SourceBean sourceBean;

    @Override
    public void save(OrganInput input) {
        try {
            log.debug(COMMENT + "SAVE ORGANIZATION" + COMMENT);
            log.debug(COMMENT + new ObjectMapper().writeValueAsString(input) + COMMENT);
            sourceBean.publishOrgChange("SAVE", input.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void update(OrganInput input) {
        try {
            log.debug(COMMENT + "UPDATE ORGANIZATION" + COMMENT);
            log.debug(COMMENT + new ObjectMapper().writeValueAsString(input) + COMMENT);
            sourceBean.publishOrgChange("UPDATE", input.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
