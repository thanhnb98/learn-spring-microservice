//package com.thanhnb.organizationservice.bean;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cloud.context.config.annotation.RefreshScope;
//import org.springframework.stereotype.Component;
//
//@Component
//@RefreshScope
////@ConfigurationProperties("limits-service")
//public class ConfigProperties {
//    @Value("${maximum}")
//    private int maximum;
//
//    @Value("${minimum}")
//    private int minimum;
//
//    public int getMaximum() {
//        return maximum;
//    }
//
//    public void setMaximum(int maximum) {
//        this.maximum = maximum;
//    }
//
//    public int getMinimum() {
//        return minimum;
//    }
//
//    public void setMinimum(int minimum) {
//        this.minimum = minimum;
//    }
//}
