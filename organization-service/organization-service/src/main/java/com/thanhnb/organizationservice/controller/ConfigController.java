//package com.thanhnb.organizationservice.controller;
//
//import com.thanhnb.organizationservice.bean.ConfigProperties;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class ConfigController {
//
//    @Autowired
//    private ConfigProperties configProperties;
//
//    @RequestMapping(value = "/get-configs", method = RequestMethod.GET)
//    public LimitConfigOutput getConfigs() {
//        return new LimitConfigOutput(configProperties.getMaximum(), configProperties.getMinimum());
//    }
//}
//
//class LimitConfigOutput {
//    private int maximum;
//    private int minimum;
//
//    public LimitConfigOutput(int maximum, int minimum) {
//        this.maximum = maximum;
//        this.minimum = minimum;
//    }
//
//    public int getMaximum() {
//        return maximum;
//    }
//
//    public void setMaximum(int maximum) {
//        this.maximum = maximum;
//    }
//
//    public int getMinimum() {
//        return minimum;
//    }
//
//    public void setMinimum(int minimum) {
//        this.minimum = minimum;
//    }
//}
