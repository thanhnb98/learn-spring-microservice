package com.thanhnb.organizationservice.events.source;

import com.thanhnb.organizationservice.events.models.OrganizationChangeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;


@Component
@EnableBinding(CustomerChannel.class)
public class SourceBean {
    private static final String COMMENT = "################################";
    private static final Logger log = LoggerFactory.getLogger(SourceBean.class);

    @Autowired
    private CustomerChannel customerChannel;

    @Autowired
    public SourceBean() {
    }

    public void publishOrgChange(String action, String orgId) {
        log.info(COMMENT + "Sending Kafka message: " + action + " for Organization id: " + orgId + COMMENT);

        //TODO: build model change
        OrganizationChangeModel changeModel = new OrganizationChangeModel();
        changeModel.setTypeName(OrganizationChangeModel.class.getTypeName());
        changeModel.setAction(action);
        changeModel.setOrgId(orgId);
        changeModel.setRequestId("NGL" + System.currentTimeMillis());

        //TODO: send message
        customerChannel.outboundOrg().send(MessageBuilder
                .withPayload(changeModel)
                .build());
    }
}
