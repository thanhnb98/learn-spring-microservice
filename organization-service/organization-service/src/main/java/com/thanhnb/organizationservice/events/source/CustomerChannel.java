package com.thanhnb.organizationservice.events.source;


import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CustomerChannel {

    @Output("outboundOrgChanges")
    MessageChannel outboundOrg();
}
