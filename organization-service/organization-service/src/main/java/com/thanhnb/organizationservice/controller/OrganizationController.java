package com.thanhnb.organizationservice.controller;

import com.thanhnb.organizationservice.inputs.OrganInput;
import com.thanhnb.organizationservice.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/org")
public class OrganizationController {

    @Autowired
    private IOrganizationService organizationService;

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public ResponseEntity<?> save() {
        OrganInput input = new OrganInput();
        input.setId("ID-12345");
        input.setField1("Field1");
        input.setField2("Field2");
        input.setField3("Field3");
        organizationService.save(input);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ResponseEntity<?> update() {
        OrganInput input = new OrganInput();
        input.setId("ID-12345");
        input.setField1("Field1");
        input.setField2("Field2");
        input.setField3("Field3");
        organizationService.update(input);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
