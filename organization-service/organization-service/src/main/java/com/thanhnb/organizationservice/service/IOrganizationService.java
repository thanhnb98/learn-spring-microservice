package com.thanhnb.organizationservice.service;

import com.thanhnb.organizationservice.inputs.OrganInput;

public interface IOrganizationService {
    void save(OrganInput input);
    void update(OrganInput input);
}
