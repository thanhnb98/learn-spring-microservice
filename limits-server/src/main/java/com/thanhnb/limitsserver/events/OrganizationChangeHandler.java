package com.thanhnb.limitsserver.events;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thanhnb.limitsserver.events.models.OrganizationChangeModel;
import com.thanhnb.limitsserver.redis.IRedisRepository;
import com.thanhnb.limitsserver.redis.Organization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(CustomChannel.class)
public class OrganizationChangeHandler {

    private static final String COMMENT = "####################################################";
    private static final Logger logger = LoggerFactory.getLogger(OrganizationChangeHandler.class);

    @Autowired
    private IRedisRepository iRedisRepository;

    /**
     * @createBy thanhnb
     * @implNote With the @StreamListener annotation, passed in ther name of your channel, "inboundOrgChanges"
     * instead of using Sink.INPUT
     * This method executed every time message send to "inboundOrgChanges" channel
     */
    @StreamListener(target = CustomChannel.INPUT)
    public void loggerSink(OrganizationChangeModel changeModel) throws JsonProcessingException {
        System.out.println("HELLO WORLD");
        switch (changeModel.getAction()) {
            case "UPDATE":
                logger.info(COMMENT + " REVERT A MESSAGE UPDATE FROM inboundOrgChanges CHANNEL " + COMMENT);
                logger.info(new ObjectMapper().writeValueAsString(changeModel));
                Organization o = new Organization();
                o.setField1("Field1");
                o.setField2("UPDATE");
                o.setField3("UPDATE");
                o.setField4("UPDATE");
                iRedisRepository.updateOrganization(o);
                break;
            case "DELETE":
                logger.info(COMMENT + " REVERT A MESSAGE DELETE FROM inboundOrgChanges CHANNEL " + COMMENT);
                logger.info(new ObjectMapper().writeValueAsString(changeModel));
                iRedisRepository.deleteOrganization(changeModel.getOrgId());
                break;
        }
    }
}
