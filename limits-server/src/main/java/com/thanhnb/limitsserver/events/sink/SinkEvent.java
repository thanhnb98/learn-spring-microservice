package com.thanhnb.limitsserver.events.sink;

import com.thanhnb.limitsserver.events.models.OrganizationChangeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
public class SinkEvent {
    private static final String COMMENT = "################################";
    private static final Logger log = LoggerFactory.getLogger(SinkEvent.class);

    /**
     * ********************************************************************
     * ********************************************************************
     * This method execute every time  a message reverted off input channel
     *
     * @param changeModel: message reverted from topic
     */
    @StreamListener(Sink.INPUT)
    public void loggerSink(OrganizationChangeModel changeModel) {
        log.info(COMMENT + "Revert a event for organization Id: " + changeModel.getOrgId());
    }
}
