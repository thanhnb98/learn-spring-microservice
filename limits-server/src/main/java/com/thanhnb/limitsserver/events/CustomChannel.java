package com.thanhnb.limitsserver.events;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomChannel {

    String INPUT = "inboundOrgChanges";

    @Input(INPUT)
    SubscribableChannel orgs();
}
