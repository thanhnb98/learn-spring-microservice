package com.thanhnb.limitsserver.controllers;

import com.thanhnb.limitsserver.redis.Organization;
import com.thanhnb.limitsserver.redis.OrganizationRestTemplateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/demo-client-loadBalancing")
public class DemoClientLoadBalancing {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OrganizationRestTemplateClient organizationRestTemplateClient;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void getOrganInfo() {
        System.out.println("========================================>");
        ResponseEntity<String> restExchange = restTemplate.exchange(
                "http://organization-service/v1/organizations/{organizationId}", HttpMethod.GET,
                null, String.class, "organizationId");
        System.out.println("RESPONSE =>" + restExchange.getBody());
    }

    @RequestMapping(value = "/info2", method = RequestMethod.GET)
    public ResponseEntity<Organization> getOrganInfo2() {
        System.out.println("========================================>");
        Organization organization = organizationRestTemplateClient.getOrganizationObject("Field1");
        return new ResponseEntity<>(organization, HttpStatus.OK);
    }
}
