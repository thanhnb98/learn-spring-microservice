package com.thanhnb.limitsserver.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class DemoDiscoveryClient {

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping(value = "/discovery-client")
    public void getOrganServiceInfo() {
        RestTemplate restTemplate = new RestTemplate();
        List<ServiceInstance> instances = discoveryClient.getInstances("organization-service");
        if (instances.size() == 0) System.out.println("NO SERVICE INSTANCE");
        System.out.println(instances.get(0).getUri().toString());
        String serviceUrl = instances.get(0).getUri().toString() + "/v1/organizations/organizationID";
        ResponseEntity<String> resExchange = restTemplate.exchange(serviceUrl, HttpMethod.GET, null, String.class);
        System.out.println("RESPONSE: " + resExchange.getBody());
    }
}
