package com.thanhnb.limitsserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;

@SpringBootApplication
@EnableDiscoveryClient
@EnableBinding(Sink.class) // Sink mean handle each message coming topic
public class LimitsServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LimitsServerApplication.class, args);
	}

}
