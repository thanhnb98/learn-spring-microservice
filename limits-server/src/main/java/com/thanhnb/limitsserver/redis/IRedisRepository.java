package com.thanhnb.limitsserver.redis;

public interface IRedisRepository {
    void saveOrganization(Organization org);

    void updateOrganization(Organization org);

    void deleteOrganization(String organizationId);

    Organization findOrganization(String organizationId);
}
