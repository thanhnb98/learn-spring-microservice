package com.thanhnb.limitsserver.redis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Component
public class OrganizationRestTemplateClient {

    private static final String COMMENT = "#######################";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IRedisRepository redisRepository;

    private static final Logger logger = LoggerFactory.getLogger(OrganizationRestTemplateClient.class);

    private Organization checkRedisCache(String organId) {
        try {
            return redisRepository.findOrganization(organId);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }

    public void cacheOrganizationObject(Organization org) {
        try {
            logger.info(COMMENT + " CACHED ORGANIZATION OBJECT " + COMMENT);
            redisRepository.saveOrganization(org);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public Organization getOrganizationObject(String organId) {
        try {
            logger.info(COMMENT + " GET ORGANIZATION OBJECT FROM REDIS" + COMMENT);
            Organization o = checkRedisCache(organId);
            //TH1: Nếu data tồn tại trong redis thì return giá trị trong redis
            if (!Objects.isNull(o)) {
                logger.info(COMMENT + " DATA FROM REDIS" + new ObjectMapper().writeValueAsString(o) + COMMENT);
                return o;
            }
            //TH2: Nếu data không tồn tại trong Redis thì make request to Organization service và cached lại data từ response
            ResponseEntity<Organization> restExchange = restTemplate.exchange(
                    "http://organization-service/v1/organizations/getOrganInfo2/{organizationId}", HttpMethod.GET,
                    null, Organization.class, "organizationId");

            o = restExchange.getBody();
            if (!Objects.isNull(o)) {
                cacheOrganizationObject(o);
            }
            return o;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
