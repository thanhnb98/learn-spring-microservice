1. To use Redis in Spring you need to do four things:
    - Config service to include the Spring Data Redis dependencies
    - Construct a database connection to Redis
    - Define the Spring Data Redis Repositories that your code will interact with Redis hash
    - Use Redis and the service to store anh read data from Redis