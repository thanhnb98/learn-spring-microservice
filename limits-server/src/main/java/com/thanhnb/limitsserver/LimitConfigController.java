package com.thanhnb.limitsserver;

import com.thanhnb.limitsserver.bean.ConfigProperties;
import com.thanhnb.limitsserver.input.LimitConfigOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LimitConfigController {

    @Autowired
    private ConfigProperties configProperties;

    @RequestMapping(value = "/get-configs", method = RequestMethod.GET)
    public LimitConfigOutput getConfigs() {
        return new LimitConfigOutput(configProperties.getMaximum(), configProperties.getMinimum());
    }

    @RequestMapping(value = "/get-configs2", method = RequestMethod.GET)
    public LimitConfigOutput getConfigs2() {
        return new LimitConfigOutput(configProperties.getMaximum(), configProperties.getMinimum());
    }

    @RequestMapping(value = "/get-configs3", method = RequestMethod.GET)
    public LimitConfigOutput getConfigs3() {
        return new LimitConfigOutput(configProperties.getMaximum(), configProperties.getMinimum());
    }
}